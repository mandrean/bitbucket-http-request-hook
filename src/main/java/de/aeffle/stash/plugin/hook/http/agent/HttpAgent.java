/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.http.agent;

import java.io.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import de.aeffle.stash.plugin.hook.http.location.HttpLocation;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.*;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;

public class HttpAgent {
    private static final Logger log = LoggerFactory.getLogger(HttpAgent.class);

    private static final String USER_AGENT = "Mozilla/5.0";
    private static final int CONNECTION_TIMEOUT_MILLIS = 500;
    private static final int SOCKET_TIMEOUT_MILLIS = 500;


    private final String urlString;
    private final boolean isSslValidationDisabled;

    private final String httpMethod;
    private final String postContentType;
    private final String postData;

	private final String user;
	private final String pass;
	private final Boolean useAuth;


    public HttpAgent(HttpLocation httpLocation) {
        urlString = httpLocation.getUrl();
        isSslValidationDisabled = httpLocation.isSslValidationDisabled();

        httpMethod = httpLocation.getHttpMethod();
        postContentType = httpLocation.getPostContentType();
		postData = httpLocation.getPostData();

        useAuth = httpLocation.isAuthEnabled();
        user = httpLocation.getUser();
	    pass = httpLocation.getPass();
	    
		log.info("Http request with URL: " + urlString + " (" + httpMethod + ")");
	}



	public Result doRequest() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        Request request = getRequest();

        request.connectTimeout(CONNECTION_TIMEOUT_MILLIS)
               .socketTimeout(SOCKET_TIMEOUT_MILLIS)
               .addHeader("User-Agent", USER_AGENT);

        if (useAuth) {
            addAuthToRequest(request);
        }


        HttpClient httpClient = isSslValidationDisabled ? getSslValidationDisabledClient() : getDefaultHttpClient();

        HttpResponse response = Executor.newInstance(httpClient)
                                        .execute(request)
                                        .returnResponse();

        int responseCode = response.getStatusLine().getStatusCode();
        String content = getContentFromHttpResponse(response);

        if (responseCode < 200 || responseCode >= 300) {
            log.error("Problem with the HTTP connection with response code: " + responseCode);
        }
        log.debug("HTTP response:\n" + content);

        return new Result(responseCode, content);
	}

    private String getContentFromHttpResponse(HttpResponse response) throws IOException {
        String content = "N/A";
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            InputStream contentStream = entity.getContent();
            if (contentStream != null) {
                content = IOUtils.toString(response.getEntity().getContent());
            }
        }
        return content;
    }

    private CloseableHttpClient getDefaultHttpClient() {
        return HttpClientBuilder.create().build();
    }

    private void addAuthToRequest(Request request) {
        String authString = user + ":" + pass;
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
        request.addHeader("Authorization", "Basic " + authStringEnc);
    }

    private Request getRequest() {
        Request request = Request.Get(urlString);

        if (httpMethod != null) {
            switch (httpMethod) {
                case "POST":
                    log.info("Do a POST Request");
                    request = Request.Post(urlString)
                                     .bodyString(postData, getContentType());
                    break;
                case "PUT":
                    log.info("Do a PUT Request");
                    request = Request.Put(urlString)
                                     .bodyString(postData, getContentType());
                    break;
                default:
                    log.info("Do a GET Request");
                    request = Request.Get(urlString);
            }
        }
        return request;
    }


    private ContentType getContentType() {
        if (postContentType.equals("application/json")) {
            return ContentType.APPLICATION_JSON;
        }
        if (!postContentType.isEmpty()) {
            return ContentType.create(postContentType);
        }

        return ContentType.APPLICATION_FORM_URLENCODED;
    }

    public HttpClient getSslValidationDisabledClient()
            throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContextBuilder.create()
                .loadTrustMaterial(null,
                        (TrustStrategy) (chain, authType) -> true)
                .build();

        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

        return HttpClientBuilder.create()
                .setSSLContext(sslContext)
                .setConnectionManager(connectionManager)
                .build();
    }


}
