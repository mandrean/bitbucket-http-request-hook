package de.aeffle.stash.plugin.hook.persistence.ao;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import java.util.Date;


@Table("AVR_AUDIT_LOG")
@Preload
public interface AuditLog extends Entity {
    /**
     * REQUEST_DATE
     */
    @NotNull
    @Accessor("REQUEST_DATE")
    Date getRequestDate();

    @Mutator("REQUEST_DATE")
    void setRequestDate(Date REQUEST_DATE);


    /**
     * REPO_ID
     */
    @NotNull
    @Accessor("REPO_ID")
    Integer getRepositoryId();

    @Mutator("REPO_ID")
    void setRepositoryId(Integer REPO_ID);


    /**
     * URL
     */
    @NotNull
    @Accessor("URL")
    String getUrl();

    @Mutator("URL")
    @StringLength(StringLength.UNLIMITED)
    void setUrl(String url);


    /**
     *  RESPONSE_CODE
     */
    @NotNull
    @Accessor("RESPONSE_CODE")
    Integer getResponseCode();

    @Mutator("RESPONSE_CODE")
    void setResponseCode(Integer RESPONSE_CODE);


    /**
     *  RESPONSE_MESSAGE
     */
    @Accessor("RESPONSE_MESSAGE")
    String getResponseMessage();

    @Mutator("RESPONSE_MESSAGE")
    @StringLength(StringLength.UNLIMITED)
    void setResponseMessage(String RESPONSE_MESSAGE);
}
