HTTP-Request Hook for Bitbucket Server
======================================

[Plugin in the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.aeffle.stash.plugin.stash-http-get-post-receive-hook/server/overview)


TL;DR
-----

It's a Bitbucket Server Repository Hook to trigger any HTTP Request 
(GET, POST, PUT) that also supports basic authentication.


My story of this plugin is as follows:
---------------------------------------

I stumbled over a problem were I wanted to have a post-receive hook to trigger some Jenkins without having a clone of this repository laying around. The Stash Jenkins plugin (Stash Post-Receive Webhook to Jenkins) that is available is nice but unfortunately has the requirement that I have to have a clone of that repository laying around. 

With this simple plugin you can trigger Jenkins by doing the following and not having to clone the repo:

1. Get your API-Token in the Jenkins user settings
2. Configure the URL in the Stash Plugin (http://jenkins.url/job/my_job_title/build
3. Set the username to your username
4. Set the password to the API-Token key
5. Enjoy!





Enable debug logging:
---------------------

### Via logback.xml ###
To enable more debug output add the following to the file:

 `./stash/webapp/WEB-INF/classes/logback.xml`


```
<!-- Turn on maximum logging for HttpGetPostReceiveHook plugin -->
<logger name="de.aeffle.stash.plugin.hook.HttpGetPostReceiveHook" level="DEBUG"/>
```



### Via REST call ###


* [Link to the 'log settings' section of the Bitbucket REST API](https://confluence.atlassian.com/bitbucketserver/bitbucket-server-debug-logging-776640147.html)

You can use the [REST API Browser](https://marketplace.atlassian.com/plugins/com.atlassian.labs.rest-api-browser/server/overview) for this with the logger name: "_de.aeffle.stash.plugin.hook.HttpGetPostReceiveHook_".

![REST API Browser](https://bitbucket.org/repo/7x7xzR/images/1327898735-bitbucket-change-log-levels.PNG)



URL translation:
----------------


To be able to add additional information to the URL the following templating strings can be used:

### User: ###
* ${user.displayName} (e.g. John Doe)
* ${user.name} (e.g. john.doe)
* ${user.email} (e.g. john@doe.com)

### Repository: ###
* ${repository.id}
* ${repository.name}
* ${repository.slug}

### Project: ###
* ${project.name}
* ${project.key}

### RefChange: ###
* ${refChange.refId} (e.g. refs/heads/master or refs/tags/myTag)
* ${refChange.name} (e.g. master or myTag)
* ${refChange.fromHash}
* ${refChange.toHash}
* ${refChange.type} (UPDATE, ADD or DELETE)


e.g. A push from user john.doe and the URL http://doe.com/${user.name} will trigger http://doe.com/john.doe.



Develop
-------

[Atlassian Developer Documentation](https://developer.atlassian.com/bitbucket/server/docs/latest/)


```bash
$ atlas-debug -u 6.3.0
```

...


```bash
docker run -d -p 3128:3128 minimum2scp/squid

export http_proxy=http://127.0.0.1:3128
curl http://example.com/
```


```
URL="http://localhost:7990/bitbucket"
PROJECT="PROJECT_1"
REPO="rep_1"

curl "${URL}/rest/api/latest/projects/${PROJECT}/repos/${REPO}/settings/hooks/de.aeffle.stash.plugin.stash-http-get-post-receive-hook%3Ahttp-get-post-receive-hook/enabled" 
-X PUT 
-H 'Content-Type: application/json' 
-H 'Accept: application/json' 
--data-binary '{"version":"3","locationCount":"1","httpMethod":"GET","url":"https://google.com/search?q=test","postContentType":"application/x-www-form-urlencoded","branchFilter":"","tagFilter":"","userFilter":""}' 
--compressed

```
